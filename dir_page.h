#ifndef DIR_PAGE_H
#define DIR_PAGE_H

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#include "server.h"

void write_dir(int fd, char *dirname);

#endif
