#include <signal.h>
#include <sys/wait.h>

#include "server.h"

void childdied(int sig) {
	while (waitpid(-1, NULL, WNOHANG) > 0);
	signal(SIGCHLD, childdied);
}

int main(int argc, char **argv) {
	signal(SIGCHLD, childdied);
	load_my_path();
	start_server();
}
