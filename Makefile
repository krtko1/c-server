#program name
PROGRAM=server
#build file
OBJDIR = build

#c++ compiler
CC = gcc
CFLAGS = -g3 -Wall -c
CSOURCES := $(wildcard *.c)
COBJECTS := $(CSOURCES:%.c=$(OBJDIR)/%.o)

#linker
LINKER = gcc
LFLAGS = -lmagic

# colors
Color_Off='\033[0m'
Black='\033[1;30m'
Red='\033[1;31m'
Green='\033[1;32m'
Yellow='\033[1;33m'
Blue='\033[1;34m'
Purple='\033[1;35m'
Cyan='\033[1;36m'
White='\033[1;37m'


all: $(PROGRAM)

$(PROGRAM): $(COBJECTS) $(NOBJECTS)
	@$(LINKER) $(COBJECTS) $(NOBJECTS) -o $@ $(LFLAGS)
	@echo -e $(Yellow)"Linking complete!"$(Color_Off)

$(COBJECTS): $(OBJDIR)/%.o : %.c
	@mkdir -p $(OBJDIR)
	@echo -e $(Blue)"C++ compiling "$(Purple)$<$(Color_Off)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo -e $(Blue)"C++ compiled "$(Purple)$<$(Blue)" successfully!"$(Color_Off)

clean:
	@rm -f $(PROGRAM) $(COBJECTS)
	@echo -e $(Cyan)"Cleaning Complete!"$(Color_Off)
