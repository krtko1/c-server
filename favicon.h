#ifndef FAVICON_H
#define FAVICON_H

#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "server.h"

void write_favicon(int fd);

#endif
