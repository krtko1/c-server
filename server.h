#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <magic.h>

#include "favicon.h"
#include "error_page.h"
#include "dir_page.h"
#include "file.h"


#define BUFSIZE 8096

extern char my_path[PATH_MAX];

void load_my_path(void);

char *get_file_name(char *buffer);

void answer(int fd);

void start_server(void);

#endif
