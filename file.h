#ifndef FILE_H
#define FILE_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "server.h"

void write_file(int fd, char *filename, char *filetype);

#endif
