#ifndef ERROR_PAGE_H
#define ERROR_PAGE_H

#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "server.h"

void write_error(int fd);

#endif
