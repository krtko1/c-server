#include "server.h"

char my_path[PATH_MAX];

void load_my_path(void) {
	realpath(".", my_path);
}

char *get_file_name(char *buffer) {
	char *filename = (char *)malloc(PATH_MAX);
	char *requested_name;
	if(!strncmp(buffer, "GET",3) || !strncmp(buffer, "get",3)){
		requested_name = strtok(buffer, " ");
		requested_name = strtok(NULL, " ");
	}
    realpath(requested_name+1, filename);
    if(strncmp(filename, my_path,strlen(my_path))){
		strcpy(filename, my_path);
	}
	return filename;
}

void answer(int fd) {
	long ret;
	static char buffer[BUFSIZE+1]; /* static so zero filled */
	ret =read(fd,buffer,BUFSIZE); 	/* read Web request in one go */
	if(ret > 0 && ret < BUFSIZE)	/* return code is valid chars */
		buffer[ret]=0;		/* terminate the buffer */
	else buffer[0]=0;

	char *filename = get_file_name(buffer);

	struct magic_set *magic = magic_open(MAGIC_MIME|MAGIC_CHECK);
	magic_load(magic,NULL);

	char *filetype = (char *)magic_file(magic,filename);

	if(!strncmp(filename+strlen(my_path),"/favicon.ico",12)){
		write_favicon(fd);
	} else if(!strncmp(filetype,"cannot open",11)){
		write_error(fd);
	} else if(!strncmp(filetype,"inode/directory",15)){
		write_dir(fd, filename);
	} else {
		write_file(fd, filename, filetype);
	}

	free(filename);
	close(fd);
}

void start_server() {
	int pid = 0;
	int one = 1;
	long long listenfd, socketfd;
	socklen_t length;
	static struct sockaddr_in cli_addr; /* static = initialised to zeros */
	static struct sockaddr_in serv_addr; /* static = initialised to zeros */

	listenfd = socket(AF_INET, SOCK_STREAM,0);
	if (listenfd == -1) {
		perror("Unable to open TCP socket\n\tError");
	}

	if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1) {
		perror("Unable to set socket to reuse address\n\tError");
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(3000);

	if (bind(listenfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) == -1) {
		perror("Unable to bind server address to socket\n\tError");
	}

	if (listen(listenfd,128) == -1) {
		perror("Unable to start listen on TCP socket\n\tError");
	}

	length = sizeof(cli_addr);
	while(1){
		socketfd = accept(listenfd, (struct sockaddr *)&cli_addr, &length);
		if(socketfd == -1) {
			perror("Unable to accept connection\n\tError");
			continue;
		}
		pid = fork();
		if(pid == 0){
			answer(socketfd);
			return;
		} else if(pid == -1){
			perror("Unable to fork\n\tError");
			answer(socketfd);
		}
	}
}
