#include "file.h"

void write_file(int fd, char *filename, char *filetype) {
	static char buffer[BUFSIZE+1];
	FILE *f = fopen(filename, "r");
	if (f == NULL) {
		perror("Unable to open file\n\tError");
		return;
	}
	if (fseek(f, 0, SEEK_END) == -1) {
		perror("Unable to seek file\n\tError");
		return;
	}
	int fsize = ftell(f);
	if (fsize == -1) {
		perror("Unable to use ftell\n\tError");
		return;
	}
	rewind(f);

	sprintf(buffer, "HTTP/1.1 200 OK\nServer: c-server\nContent-Length: %d\nAccess-Control-Allow-Origin: *\nConnection: close\nContent-Type: %s\n\n", fsize, filetype); /* Header + a blank line */
	if (write(fd, buffer, strlen(buffer)) == -1){
		perror("Write was unsuccessful\n\tError");
	}

	while((fsize = fread(buffer, 1, sizeof(buffer), f)) > 0) {
		if (write(fd, buffer, fsize) == -1){
			perror("Write was unsuccessful\n\tError");
		}
	}

	fclose(f);
}
